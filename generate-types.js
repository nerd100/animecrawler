const {readFileSync, writeFileSync} = require('fs');
const yaml = require('js-yaml');
const openapiTS = require('openapi-typescript').default;


const input = yaml.load(readFileSync('server/common/api.yml', 'utf8'));
const output = openapiTS(input, {
    formatter: (node) => {
        if(node.type === 'lokaref_id'){
            return 'string';
        }
    }
});
writeFileSync('server/api/interfaces.api.ts', output);
