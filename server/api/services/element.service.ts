import { Knex } from 'knex';
import L from '../../common/logger';

export class ElementService<T> {
  async GetById(
    knex: Knex,
    tableName: string,
    params: {
      required: [string, string];
    }
  ): Promise<any> {
    L.info('fetching ' + tableName);
    try {
      const query = knex(tableName)
        .select()
        .where(params.required[0], params.required[1])
        .first();
      return query;
    } catch (err: any) {
      L.error(err);
      throw err;
    }
  }

  async GetAll(knex: Knex, tableName: string): Promise<T[]> {
    L.info('fetching ' + tableName);
    try {
      const query = knex<any, any>(tableName).select().orderBy('Id', 'asc');
      return query;
    } catch (err: any) {
      L.error(err);
      throw err;
    }
  }

  async Insert<T>(knex: Knex, tableName: string, data: T): Promise<any[]> {
    L.info('fetching ' + tableName);
    try {
      const query = knex.insert(data).into(tableName);
      return query;
    } catch (err: any) {
      L.error(err);
      throw err;
    }
  }

  async UpdateObject<T>(
    knex: Knex,
    tableName: string,
    id: number,
    data: T
  ): Promise<any> {
    L.info('update ' + tableName);
    try {
      const query = knex(tableName)
        .where({ Id: id })
        .update(data);
      return query;
    } catch (err: any) {
      L.error(err);
      throw err;
    }
  }

  async Delete(knex: Knex, tableName: string, id: string): Promise<any> {
    L.info('delete with id: ' + id + ' from table ' + tableName);
    try {
      const query = knex(tableName).where({ Id: id }).del();
      return query;
    } catch (err: any) {
      L.error(err);
      throw err;
    }
  }

  async UpdateColumn(
    knex: Knex,
    tableName: string,
    params: {
      id: number;
      updateColumn: [string, string];
    }
  ): Promise<any> {
    L.info('update ' + tableName);
    try {
      const query = knex(tableName)
        .where({ Id: params.id })
        .update(params.updateColumn[0], params.updateColumn[1]);
      return query;
    } catch (err: any) {
      L.error(err);
      throw err;
    }
  }

  async Exists(
    knex: Knex,
    tableName: string,
    columns: [string, string][]
  ): Promise<any> {
    L.info('fetching ' + tableName);
    try {
      const query: any = knex(tableName)
        .count('Id', { as: 'count' })
        .modify(function (queryBuilder) {
          if (columns.length > 0) {
            columns.forEach((arg) => queryBuilder.andWhere(arg[0], arg[1]));
          }
        });
      return query;
    } catch (err: any) {
      L.error(err);
      throw err;
    }
  }
}
