import { Knex } from 'knex';

export const knex = require('knex')({
  client: process.env.KNEX_CLIENT || 'mysql',
  connection: {
    host: process.env.KNEX_HOST || '127.0.0.1',
    port: process.env.KNEX_PORT || '3306',
    user: process.env.KNEX_USER || 'root',
    password: process.env.KNEX_PASSWORD || 'password',
    database: process.env.KNEX_DATABASE || 'data',
  },
}) as Knex;

export const transaction = <T>(
  transactionScope: (trx: Knex.Transaction) => Promise<T> | void
): Promise<T> => {
  return knex.transaction(transactionScope);
};
