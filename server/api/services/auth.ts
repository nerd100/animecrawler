import StringCrypto from 'string-crypto';
import jwt from 'jsonwebtoken';
import { UserDetailObject } from '../dataLayer/dbTypes';

const secret: string = process.env.SECRET ?? 'fe421e54-9da7-4ca2-a772-2ff521bbfc10';
const refreshSecret: string = process.env.REFRESH_SECRET ?? '9a618117-d533-44d7-a46a-64f2cf395a3d';

export async function decrypt(
  password: string,
  encryptedPassword: string
): Promise<boolean> {
  const secret: string = process.env.SECRET ?? 'fe421e54-9da7-4ca2-a772-2ff521bbfc10';

  const { decryptString } = new StringCrypto();
  const decryptedString = decryptString(encryptedPassword, secret);

  if (password === decryptedString) {
    return true;
  } else {
    return false;
  }
}

export function createJwtToken(user: UserDetailObject): string {
  const token = jwt.sign({ data: user }, secret, {
    expiresIn: '15m',
    algorithm: 'HS256',
  });
  return token;
}

export function createJwtRefreshToken(user: UserDetailObject): string {
  const refreshToken = jwt.sign({ data: user }, refreshSecret, {
    expiresIn: '30m',
    algorithm: 'HS256',
  });
  return refreshToken;
}

export function verifyToken(token: string, secret: string): boolean {
  try {
    jwt.verify(token, secret);
    return true;
  } catch (err) {
    return false;
  }
}
