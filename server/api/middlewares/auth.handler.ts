import { Request, Response, NextFunction } from 'express';
import jwt from 'jsonwebtoken';

export default function authHandler(
  _req: Request,
  res: Response,
  _next: NextFunction
): void {
  try {
    console.log("auth")
    const token: string = _req.headers.authorization ?? '';
    const secret = process.env.SECRET ?? 'fe421e54-9da7-4ca2-a772-2ff521bbfc10';

    jwt.verify(token as string, secret as string, function (err, decoded) {
      if (err) {
        res.status(401).json({ error: err.message });
      } else {
        decoded;
        _next();
      }
    });
  } catch (err) {
    console.log('Token: ' + err);
    res.status(401).json({ error: err.message });
  }
}
