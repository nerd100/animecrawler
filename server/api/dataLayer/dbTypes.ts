export type AnimeDbObject = {
  Id: number;
  Name: string;
  Expression: string;
};

export type EpisodeDbObject = {
  AnimeId: number;
  Season: number;
  Episode: number;
  StatusId: number;
  Date: Date;
};

export type PersonDbObject = {
  Id: number;
  Vorname: string;
  Nachname: string;
  Alias: string;
  Adresse: {
    Strasse: string,
    Hausnummer: string,
    Plz: string,
    Ort: string,
  },
  Geburtstag: Date
};

export type UserDbObject = {
  Id: number;
  Username: string;
  Password: string;
};

export type UserDetailObject = {
  Id: number;
  Username: string;
  Password: string;
};

export type AnimeDetailObject = {
  Id: number;
  Name: string;
  Expression: string;
};

export type EpisodeDetailObject = {
  Id: number;
  AnimeId: number;
  Anime: string;
  Season: number;
  Episode: number;
  StatusId: number;
  Status: string;
  Date: Date;
};

export type PersonDetailObject = {
  Id: number;
  Vorname: string;
  Nachname: string;
  Alias: string;
  Strasse: string,
  Hausnummer: string,
  Plz: string,
  Ort: string,
  Geburtstag: Date
};

export type AnimeListObject = {
  list: AnimeDetailObject[];
};

export type EpisodeListObject = {
  list: EpisodeDetailObject[];
};

export type PersonListObject = {
  list: PersonDetailObject[];
};
