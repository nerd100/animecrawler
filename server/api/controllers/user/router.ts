import express from 'express';
import userController from './controller';
export default express
  .Router()
  .post('/login', userController.login.bind(userController));
