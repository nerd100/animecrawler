import { ElementService } from '../../services/element.service';
import { NextFunction, Request, Response } from 'express';
import { knex } from '../../services/db.service';
import { tableDefinitions } from '../../../common/tableDefinitions';
import { USER_OBJECT } from './user';
import { UserDbObject } from 'server/api/dataLayer/dbTypes';
import {
  createJwtRefreshToken,
  createJwtToken,
  decrypt,
} from '../../services/auth';

const userTable = tableDefinitions.userTable;

export class UserController {
  elementService = new ElementService<USER_OBJECT[]>();

  async login(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
      console.log("login")
      const requestBody: UserDbObject = req.body;
      const userObject: UserDbObject = await this.elementService.GetById(
        knex,
        userTable.tableName,
        {
          required: [userTable.username, requestBody.Username],
        }
      );

      if (userObject === undefined) {
        res.status(404).json([]);
      } else {
        const dbUser: UserDbObject = await this.map(userObject);

        //encrypt password
        const password_matched: boolean = await decrypt(
          requestBody.Password,
          dbUser.Password
        );
        
        if (password_matched) {
          const newToken = createJwtToken(dbUser);
          const newRefreshToken = createJwtRefreshToken(dbUser);
          res.status(200).json({
            user: dbUser,
            jwtToken: newToken,
            refreshJwtToken: newRefreshToken,
          });
        } else {
          res.status(404).json({ error: 'Password wrong!' });
        }
      }
    } catch (err) {
      return next(err);
    }
  }

  async map(dbObject: USER_OBJECT): Promise<UserDbObject> {
    const returnObject: UserDbObject = {
      Id: dbObject.Id,
      Username: dbObject.Username,
      Password: dbObject.Password,
    };

    return returnObject;
  }
}

export default new UserController();
