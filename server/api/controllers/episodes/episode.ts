export interface EPISODE_OBJECT {
  Id: number;
  AnimeId: number;
  Anime: string;
  Season: number;
  Episode: number;
  StatusId: number;
  Status: string;
  Date: Date;
}
