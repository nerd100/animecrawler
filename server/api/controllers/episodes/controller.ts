import { ElementService } from '../../services/element.service';
import { NextFunction, Request, Response } from 'express';
import { knex } from '../../services/db.service';
import { tableDefinitions } from '../../../common/tableDefinitions';
import { EPISODE_OBJECT } from './episode';
import {
  EpisodeListObject,
  EpisodeDetailObject,
} from '../../dataLayer/dbTypes';

const episodeTable = tableDefinitions.episodeTable;

export class EpisodesController {
  elementService = new ElementService<EPISODE_OBJECT>();

  async details(
    req: Request,
    res: Response,
    next: NextFunction
  ): Promise<void> {
    try {
      const episodeId: string =
        req?.query.id === undefined ? '' : req.query.id.toString();

      const episodeObject: EPISODE_OBJECT = await this.elementService.GetById(
        knex,
        episodeTable.tableName,
        {
          required: [episodeTable.id, episodeId],
        }
      );

      if (episodeObject === undefined) {
        res.status(404).json([]);
      } else {
        const returnObject: EPISODE_OBJECT = episodeObject;
        res.json(returnObject);
      }
    } catch (err) {
      return next(err);
    }
  }

  async update(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
      const requestBody: EPISODE_OBJECT = req.body;
      const result: any = await this.elementService.UpdateColumn(
        knex,
        episodeTable.tableName,
        {
          id: requestBody.Id,
          updateColumn: [
            episodeTable.statusId,
            requestBody.StatusId.toString(),
          ],
        }
      );

      if (result === 0) {
        res.status(404).json(requestBody);
      } else {
        res.status(200).json(requestBody);
      }
    } catch (err) {
      return next(err);
    }
  }

  async list(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
      req;
     
      const episodeList: EPISODE_OBJECT[] = await this.elementService.GetAll(
        knex,
        episodeTable.tableView
      );
      console.log(episodeList)
      if (episodeList === undefined) {
        res.status(404).json([]);
      } else {
        const returnObject: EpisodeListObject = await this.mapList(episodeList);
        res.status(200).json(returnObject);
      }
    } catch (err) {
      return next(err);
    }
  }

  async mapList(dbObject: EPISODE_OBJECT[]): Promise<EpisodeListObject> {
    const returnObject: EpisodeListObject = {
      list: [],
    };

    for (const ii in dbObject) {
      returnObject.list[ii] = await this.map(dbObject[ii]);
    }
    return returnObject;
  }

  async map(dbObject: EPISODE_OBJECT): Promise<EpisodeDetailObject> {
    const returnObject: EpisodeDetailObject = {
      Id: dbObject.Id,
      AnimeId: dbObject.AnimeId,
      Anime: dbObject.Anime,
      Season: dbObject.Season,
      Episode: dbObject.Episode,
      StatusId: dbObject.StatusId,
      Status: dbObject.Status,
      Date: dbObject.Date
    };

    return returnObject;
  }
}
export default new EpisodesController();
