import express from 'express';
import authHandler from '../../../api/middlewares/auth.handler';
import episodeController from './controller';
export default express
  .Router()
  .get('/episodes/details', episodeController.details.bind(episodeController))
  .get('/episodes/list', episodeController.list.bind(episodeController))
  .post('/episodes/update', episodeController.update.bind(episodeController));
