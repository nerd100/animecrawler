import express from 'express';
import authHandler from '../../../api/middlewares/auth.handler';
import personController from './controller';
export default express
  .Router()
  .get('/person/details', personController.details.bind(personController))
  .get('/person/list', personController.list.bind(personController))
  .post('/person/insert', personController.insert.bind(personController))
  .post('/person/update', personController.update.bind(personController))
  .delete('/person/delete', personController.delete.bind(personController));
