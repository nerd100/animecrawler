export interface PERSON_OBJECT {
  Id: number;
  Vorname: string;
  Nachname: string;
  Alias: string;
  Strasse: string,
  Hausnummer: string,
  Plz: string,
  Ort: string,
  Geburtstag: Date
}
