import { ElementService } from '../../services/element.service';
import { NextFunction, Request, Response } from 'express';
import { knex } from '../../services/db.service';
import { tableDefinitions } from '../../../common/tableDefinitions';
import { PERSON_OBJECT } from './person';
import {
  PersonDetailObject,
  PersonListObject,
} from 'server/api/dataLayer/dbTypes';

const personTable = tableDefinitions.personTable;

export class PersonController {
  elementService = new ElementService<PERSON_OBJECT>();

  async details(
    req: Request,
    res: Response,
    next: NextFunction
  ): Promise<void> {
    try {
      const personId: string =
        req?.query.id === undefined ? '' : req.query.id.toString();

      const personObject: PERSON_OBJECT = await this.elementService.GetById(
        knex,
        personTable.tableName,
        {
          required: [personTable.id, personId],
        }
      );
      if (personObject === undefined) {
        res.status(404).json([]);
      } else {
        const returnObject: PersonDetailObject = await this.map(personObject);
        res.json(returnObject);
      }
    } catch (err) {
      return next(err);
    }
  }

  async list(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
      console.log(req?.body)
      const personList: PERSON_OBJECT[] = await this.elementService.GetAll(
        knex,
        personTable.tableName
      );
      if (personList === undefined) {
        res.status(404).json([]);
      } else {
        const returnObject: PersonListObject = await this.mapList(personList);
        res.json(returnObject);
      }
    } catch (err) {
      return next(err);
    }
  }

  async insert(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
      console.log(req.body);

      const requestBody: PERSON_OBJECT = {
        Id: req.body.AnimeId,
        Vorname: req.body.Vorname,
        Nachname: req.body.Nachname,
        Alias: req.body.Alias,
        Strasse: req.body.Strasse,
        Hausnummer: req.body.Hausnummer,
        Plz: req.body.Plz,
        Ort: req.body.Ort,
        Geburtstag: req.body.Geburtstag        
      };

      const repsonse = await this.elementService.Insert<PERSON_OBJECT>(
        knex,
        personTable.tableName,
        requestBody
      );

      if (repsonse === undefined) {
        res.status(404).json([]);
      } else {
        res.json(repsonse);
      }
    } catch (err) {
      return next(err);
    }
  }

  async update(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
      const personId: string =
        req?.query.id === undefined ? '' : req.query.id.toString();

      let id = parseInt(personId);

      const requestBody: PERSON_OBJECT = {
        Id: req.body.Id,
        Vorname: req.body.Vorname,
        Nachname: req.body.Nachname,
        Alias: req.body.Alias,
        Strasse: req.body.Strasse,
        Hausnummer: req.body.Hausnummer,
        Plz: req.body.Plz,
        Ort: req.body.Ort,
        Geburtstag: req.body.Geburtstag       
      };

      const repsonse = await this.elementService.UpdateObject<PERSON_OBJECT>(
        knex,
        personTable.tableName,
        id,
        requestBody
      );

      if (repsonse === undefined) {
        res.status(404).json([]);
      } else {
        res.json(repsonse);
      }
    } catch (err) {
      return next(err);
    }
  }

  async delete(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
      const personId: string =
        req?.query.id === undefined ? '' : req.query.id.toString();

      const result: any = await this.elementService.Delete(
        knex,
        personTable.tableName,
        personId
      );
      if (result === 0) {
        res.status(404).json(result);
      } else {
        res.status(200).json(result);
      }
    } catch (err) {
      return next(err);
    }
  }

  async mapList(dbObject: PERSON_OBJECT[]): Promise<PersonListObject> {
    const returnObject: PersonListObject = {
      list: [],
    };

    for (const ii in dbObject) {
      returnObject.list[ii] = await this.map(dbObject[ii]);
    }
    return returnObject;
  }

  async map(dbObject: PERSON_OBJECT): Promise<PersonDetailObject> {
    const returnObject: PersonDetailObject = {
      Id: dbObject.Id,
      Vorname: dbObject.Vorname,
      Nachname: dbObject.Nachname,
      Alias: dbObject.Alias,
      Strasse: dbObject.Strasse,
      Hausnummer: dbObject.Hausnummer,
      Plz: dbObject.Plz,
      Ort: dbObject.Ort,
      Geburtstag: dbObject.Geburtstag
    };
    
    return returnObject;
  }
}

export default new PersonController();
