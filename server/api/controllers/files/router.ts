import express from 'express';
import filesController from './controller';
export default express
  .Router()
  .get('/files/file', filesController.file.bind(filesController));
