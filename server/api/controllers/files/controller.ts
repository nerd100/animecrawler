import { NextFunction, Request, Response } from 'express';

export class FilesController {

  async file(
    req: Request,
    res: Response,
    next: NextFunction
  ): Promise<void> {
    try {
      const fileId: string =
        req?.query.fileId === undefined ? '' : req.query.fileId.toString();
        console.log(fileId);
        const filePath = "./files/";
        
        if(fileId == "-1"){
          res.download(filePath +'xxx.txt');
        }else{
          res.json({error: "Couldn't find file"});
        }
    } catch (err) {
      return next(err);
    }
  }
}

export default new FilesController();
