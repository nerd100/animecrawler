import { NextFunction, Request, Response } from 'express';
import SpotifyWebApi from 'spotify-web-api-node';


export class SpotifyController {
    async song(
      req: Request,
      res: Response,
      next: NextFunction
    ): Promise<void> {
      try {
        const clientId: string =
          req?.query.clientId === undefined ? '' : req.query.clientId.toString();
        const clientSecret: string =
          req?.query.clientSecret === undefined ? '' : req.query.clientSecret.toString();
        const refreshToken: string =
          req?.query.refreshToken === undefined ? '' : req.query.refreshToken.toString();

          const scopes: string[] = ['user-read-private']
          const client_id = clientId; //11dbf778bd68488e81bf6b4c27d0585e
          const client_secret = clientSecret; //6dcb7308f6ff4c92a2e4b8f4cf1806db
          const refresh_Token = refreshToken; //AQCZbZjoiws_qDCkuEaMJrFDqMco-LeyxlhqCWQQLw7A7AlxBzVj3T7QiCrpF0IZ6p9f8JUz8PVk43ks6tfLMHFK45qXfmg5Z1kfa1S1yYK-OW0g_TgSbiBecljHTQv5Ocg

          const credentials = {
            clientId: client_id,
            clientSecret: client_secret,
            //accessToken: access_Token,
            refreshToken: refresh_Token
          };
          
          var spotifyApi = new SpotifyWebApi(credentials);
          const newToken = await spotifyApi.refreshAccessToken();

          spotifyApi.setAccessToken(newToken.body.access_token);
          const data: any = await spotifyApi.getMyCurrentPlayingTrack();

        if(data === undefined || data.statusCode !== 200){
          var errorMessage = "No Data Found";
          res.json(errorMessage);
        }else{
          const outputData = data.body.item.artists[0].name + ' - ' + data.body.item?.name + ' # ' + data.body.item?.external_urls.spotify + ' '
          const returnObject: string = outputData;
          res.json(returnObject);
        }
      } catch (err) {
        return next(err);
      }
    }
}

export default new SpotifyController();