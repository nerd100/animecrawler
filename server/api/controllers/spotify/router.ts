import express from 'express';
import spotifyController from './controller';
export default express
  .Router()
  .get('/spotify/song', spotifyController.song.bind(spotifyController));
