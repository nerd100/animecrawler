import { ElementService } from '../../services/element.service';
import { NextFunction, Request, Response } from 'express';
import { knex } from '../../services/db.service';
import { tableDefinitions } from '../../../common/tableDefinitions';
import { ANIME_OBJECT } from './anime';
import {
  AnimeDbObject,
  AnimeDetailObject,
  AnimeListObject,
} from 'server/api/dataLayer/dbTypes';

const animeTable = tableDefinitions.animeTable;

export class AnimeController {
  elementService = new ElementService<ANIME_OBJECT>();

  async details(
    req: Request,
    res: Response,
    next: NextFunction
  ): Promise<void> {
    try {
      const animeId: string =
        req?.query.id === undefined ? '' : req.query.id.toString();

      const animeObject: ANIME_OBJECT = await this.elementService.GetById(
        knex,
        animeTable.tableName,
        {
          required: [animeTable.id, animeId],
        }
      );
      if (animeObject === undefined) {
        res.status(404).json([]);
      } else {
        const returnObject: ANIME_OBJECT = animeObject;
        res.json(returnObject);
      }
    } catch (err) {
      return next(err);
    }
  }

  async list(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
      req?.query;
      const animeList: ANIME_OBJECT[] = await this.elementService.GetAll(
        knex,
        animeTable.tableName
      );
      if (animeList === undefined) {
        res.status(404).json([]);
      } else {
        const returnObject: AnimeListObject = await this.mapList(animeList); //TODO Map animes
        res.json(returnObject);
      }
    } catch (err) {
      return next(err);
    }
  }

  async insert(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
      console.log(req.body);

      const requestBody: AnimeDbObject = {
        Id: req.body.AnimeId,
        Name: req.body.Name,
        Expression: req.body.Expression,
      };

      const repsonse = await this.elementService.Insert<AnimeDbObject>(
        knex,
        animeTable.tableName,
        requestBody
      );

      if (repsonse === undefined) {
        res.status(404).json([]);
      } else {
        res.json(repsonse);
      }
    } catch (err) {
      return next(err);
    }
  }

  async delete(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
      const animeId: string =
        req?.query.id === undefined ? '' : req.query.id.toString();

      const result: any = await this.elementService.Delete(
        knex,
        animeTable.tableName,
        animeId
      );
      if (result === 0) {
        res.status(404).json(result);
      } else {
        res.status(200).json(result);
      }
    } catch (err) {
      return next(err);
    }
  }

  async mapList(dbObject: ANIME_OBJECT[]): Promise<AnimeListObject> {
    const returnObject: AnimeListObject = {
      list: [],
    };

    for (const ii in dbObject) {
      returnObject.list[ii] = await this.map(dbObject[ii]);
    }
    return returnObject;
  }

  async map(dbObject: ANIME_OBJECT): Promise<AnimeDetailObject> {
    const returnObject: AnimeDetailObject = {
      Id: dbObject.Id,
      Name: dbObject.Name,
      Expression: dbObject.Expression,
    };

    return returnObject;
  }
}

export default new AnimeController();
