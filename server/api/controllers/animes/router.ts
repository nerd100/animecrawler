import express from 'express';
import authHandler from '../../../api/middlewares/auth.handler';
import animeController from './controller';
export default express
  .Router()
  .get('/animes/details', animeController.details.bind(animeController))
  .get('/animes/list', animeController.list.bind(animeController))
  .post('/animes/insert', animeController.insert.bind(animeController))
  .delete('/animes/delete', animeController.delete.bind(animeController));
