import { NextFunction, Request, Response } from 'express';
import { UserDetailObject } from 'server/api/dataLayer/dbTypes';
import {
  createJwtToken,
  createJwtRefreshToken,
  verifyToken,
} from '../../services/auth';

export class AuthController {
  async refreshToken(
    req: Request,
    res: Response,
    next: NextFunction
  ): Promise<void> {
    try {

      const refreshSecret = process.env.REFRESH_SECRET || '9a618117-d533-44d7-a46a-64f2cf395a3d';

      const refreshtoken: any = req.body.refreshToken ?? '';
      const jwtRefreshTokenVerified = verifyToken(
        refreshtoken,
        refreshSecret
      );
      if (jwtRefreshTokenVerified) {
        const user: UserDetailObject = {Id: 1, Username: "", Password: ""};
        const newToken = createJwtToken(user);
        const newRefreshToken = createJwtRefreshToken(user);
        res
          .status(200)
          .json({ jwtToken: newToken, jwtRefreshToken: newRefreshToken });
      } else {
        res.status(500).json([]);
      }
    } catch (err) {
      return next(err);
    }
  }
}

export default new AuthController();
