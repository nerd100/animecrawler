import express from 'express';
import authController from './controller';
export default express
  .Router()
  .post('/refreshToken', authController.refreshToken.bind(authController));
