export const tableDefinitions = {
  animeTable: {
    tableName: 'animes',
    id: 'Id',
    name: 'Name',
    expression: 'expression',
  },

  episodeTable: {
    tableName: 'episodes',
    tableView: 'episodes_view',
    id: 'Id',
    animeId: 'AnimeId',
    season: 'Season',
    episode: 'Episode',
    statusId: 'StatusId',
    status: 'Status',
    date: 'Date'
  },

  userTable: {
    tableName: 'users',
    id: 'Id',
    username: 'Username',
    password: 'Password',
  },

  personTable: {
    tableName: 'person',
    id: 'Id',
    vorname: 'Vorname',
    nachname: 'Nachname',
    alias : 'Alias',
    strasse : 'Strasse',
    hausnummer: 'Hausnummer',
    plz: 'Plz',
    ort: 'Ort',
    geburtstag: 'Geburtstag'
  },
};
