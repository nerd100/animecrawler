import { Client, TextChannel } from "discord.js";

var clientBot: Client;

export class DiscordBot {

  startBot(): void {
    const token = "MTAyNjE1NDY1NTQ4ODY4ODI4OA.GyKi5Y.Xk65dvNnyLURDH1z1a_uokhKeFJWG6rSGPu2J0";
    console.log("Bot is starting...")
    const client = new Client({
      intents: []
    });
    client.login(token);
    clientBot = client;
  }

  async sendMessage(channelNames: string[], message: string): Promise<void> {
    if (!clientBot.user || !clientBot.application) {
      console.log(`$Bot is offline`)
      return;
    }
    console.log(`${clientBot.user.username} is online`)
    var guild = clientBot.guilds.cache.get("892100220232540190");
    var channels = await guild?.channels.fetch();
    var targetChannel = channels?.find(c => channelNames.includes(c?.name.split('_')[0] as string));
    (targetChannel as TextChannel).send({ content: message })
  };

  async sendFile(channelName: string, file: string): Promise<void> {
    if (!clientBot.user || !clientBot.application) {
      console.log(`$Bot is offline`)
      return;
    }
    console.log(`${clientBot.user.username} is online`)
    var guild = clientBot.guilds.cache.get("892100220232540190");
    var channels = await guild?.channels.fetch();
    var targetChannel = channels?.find(c => c?.name.includes(channelName) === true);
    (targetChannel as TextChannel).send({ files: [file] })

  };

  async sendMessageWithFile(channelName: string, message: string, file: string): Promise<void> {
    if (!clientBot.user || !clientBot.application) {
      console.log(`$Bot is offline`)
      return;
    }
    console.log(`${clientBot.user.username} is online`)
    var guild = clientBot.guilds.cache.get("892100220232540190");
    var channels = await guild?.channels.fetch();
    var targetChannel = channels?.find(c => c?.name.includes(channelName) === true);
    (targetChannel as TextChannel).send({ content: message, files: [file] })
  };

}

export default new DiscordBot();
