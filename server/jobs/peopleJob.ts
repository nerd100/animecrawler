import { ElementService } from '../api/services/element.service';
import { tableDefinitions } from '../common/tableDefinitions';
import { knex } from '../api/services/db.service';
import { PersonDbObject } from '../api/dataLayer/dbTypes';
import discordBot from '../discordBot/discordBot';

var push = require( 'pushsafer-notifications' );

const personTable = tableDefinitions.personTable;
const elementPersonService = new ElementService<PersonDbObject>();

export async function checkPeopleJob() {
  const currentPersons: PersonDbObject[] = await getPeople();
  checkBirthday(currentPersons);
}

async function getPeople(): Promise<PersonDbObject[]> {
  try {
    const personList: PersonDbObject[] = await elementPersonService.GetAll(
      knex,
      personTable.tableName
    );
    return personList;
  } catch (err) {
    console.log(err);
    return err;
  }
}

function checkBirthday(persons: PersonDbObject[]){
  persons.forEach(person => {
    var today = new Date();
    var todayIn14Days = new Date();
    var todayIn7Days = new Date();
    var todayIn3Days = new Date();
    var todayIn0Days = new Date();

    todayIn14Days.setDate(today.getDate() + 14);
    todayIn7Days.setDate(today.getDate() + 7);
    todayIn3Days.setDate(today.getDate() + 3);
    todayIn0Days.setDate(today.getDate());

    let todayIn14DaysDay = todayIn14Days.getDate();
    let todayIn14DaysMonth = todayIn14Days.getMonth() + 1;

    let todayIn7DaysDay = todayIn7Days.getDate();
    let todayIn7DaysMonth = todayIn7Days.getMonth() + 1;

    let todayIn3DaysDay = todayIn3Days.getDate();
    let todayIn3DaysMonth = todayIn3Days.getMonth() + 1;

    let todayIn0DaysDay = todayIn0Days.getDate();
    let todayIn0DaysMonth = todayIn0Days.getMonth() + 1;

    var birthday = person.Geburtstag;
    let birthdayDay = birthday.getDate();
    let birthdayMonth = birthday.getMonth() + 1;

    let nameList = [person.Vorname.toLowerCase(), person.Nachname.toLowerCase()]
    if(person.Alias !== null){
      nameList.push(person.Alias.toLowerCase())
    }

    if (todayIn14DaysDay === birthdayDay && todayIn14DaysMonth === birthdayMonth) {
      let message = (`In zwei Wochen hat ${person.Vorname} ${person.Nachname} Geburtstag.🎂🎉🥳`);
      console.log(person);
      discordBot.sendMessage(nameList, message);
    }

    if (todayIn7DaysDay === birthdayDay && todayIn7DaysMonth === birthdayMonth) {
      let message = (`In einer Wochen hat ${person.Vorname} ${person.Nachname} Geburtstag.🎂🎉🥳`);
      discordBot.sendMessage(nameList, message);
    }

    if (todayIn3DaysDay === birthdayDay && todayIn3DaysMonth === birthdayMonth) {
      let message = (`In drei Tagen hat ${person.Vorname} ${person.Nachname} Geburtstag.🎂🎉🥳`);
      discordBot.sendMessage(nameList, message);
    }

    if (todayIn0DaysDay === birthdayDay && todayIn0DaysMonth === birthdayMonth) {
      //var p = new push({
      //  k: 'hpD0FEAnDrGWDiipNaQT',
      //  debug: true
      //});

      let message = (`Heute hat ${person.Vorname} ${person.Nachname} Geburtstag.🎂🎉🥳`);
      //var msg = { m: message };
      //p.send(msg);
      discordBot.sendMessage(nameList, message);
    }
  });
}