import nodeCron from 'node-cron';
import { checkPeopleJob } from './peopleJob';
import { scrapNewEpisodesCron } from './scraper';
import { dailyDose } from './dailyDoseJob';
import discordBot from '../discordBot/discordBot';

export async function addJobs() {

  //discordBot.startBot();
  
  nodeCron.schedule('0 6 * * *', dailyDose);
  nodeCron.schedule('30 * * * *', scrapNewEpisodesCron);
  nodeCron.schedule('0 10 * * *', checkPeopleJob);
  console.log('All Jobs registerd');
}
