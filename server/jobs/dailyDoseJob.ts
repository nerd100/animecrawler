import fetch from 'node-fetch';
import discordBot from '../discordBot/discordBot';

enum CityNames {
  Magdeburg = "Magdeburg",
  Berlin = "Berlin",
  Leipzig = "Leipzig",
  Lueneburg = "Lüneburg",
  Steimke = "52.746080,10.600510",
}

let icons: Record<number, string> = {
    1000 : "☀️",
    1003 : "🌥️",
    1006 : "☁️",
    1009 : "🌥️",
    1150 : "🌧️",
    1183 : "🌧️",
    1186 : "🌧️",
    1189 : "🌧️",
    1192 : "🌧️",
    1195 : "🌧️",
    1240 : "🌧️",
    1243 : "🌧️",
    1213 : "🌨️",
    1216 : "🌨️",
    1219 : "🌨️",
    1222 : "🌨️",
    1225 : "🌨️",
    1255 : "🌨️",
    1273 : "⛈️",
    1276 : "⛈️",
    1087 : "⛈️",
    1030 : "🌫️"
};

export async function dailyDose() {
  let dogOfTheDay = await fetch("https://dog.ceo/api/breeds/image/random");
  let dogOfTheDayPic = (await dogOfTheDay.json())["message"];

  const options = {
    method: 'GET',
    headers: {
      'X-RapidAPI-Key': '59b614bd47msh7bcfbef1db2bd0ep100d34jsn8ce834a2a1c9',
      'X-RapidAPI-Host': 'weatherapi-com.p.rapidapi.com'
    }
  };

  const cities = Object.values(CityNames);
  let outputStringArray:string[] = [];

for (const city of cities){
  let weather = await fetch(`https://weatherapi-com.p.rapidapi.com/forecast.json?q=${city}&days=1&lang=de`, options);
    let weatherJSON = (await weather.json());
    let icon = icons[weatherJSON["current"]["condition"]["code"]];
    let locationName = weatherJSON["location"]["name"];
    if(city === "52.746080,10.600510")
    {
      locationName = "Steimke";
    }

    let weatherOutput = locationName + " | " + weatherJSON["current"]["temp_c"] + "°C " + weatherJSON["current"]["condition"]["text"] + " " +
      icon + " Min: " + weatherJSON["forecast"]["forecastday"][0]["day"]["mintemp_c"] + "°C Max: " +
      weatherJSON["forecast"]["forecastday"][0]["day"]["maxtemp_c"] + "°C";
      console.log(weatherOutput)
      outputStringArray.push(weatherOutput);
}

  let weatherString = outputStringArray.join("\n");

  let greetings = ["Guten Morgen ihr kleinen Engel", "Pflücke den Tag", "Du kannst es schaffen", "Guten Morgen, guten Morgen, guten Morgen, Sonnenschein",
    "Guten Morgen ihr Süßßßen", "Guten Morgen! Und falls wir uns heute nicht mehr sehen, guten Tag, guten Abend und gute Nacht!"]
  let dailyGreeting = greetings[Math.floor(Math.random() * greetings.length)];

  discordBot.sendMessageWithFile("dailydose", dailyGreeting + " #dogoftheday\n" +
    "Das ist das heutige Wetter:\n" +
    weatherString + "\n",
    dogOfTheDayPic);
}