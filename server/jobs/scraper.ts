import axios from 'axios';
import cheerio from 'cheerio';
import { ElementService } from '../api/services/element.service';
import { tableDefinitions } from '../common/tableDefinitions';
import { knex } from '../api/services/db.service';
import { AnimeDbObject, EpisodeDbObject } from '../api/dataLayer/dbTypes';

const url = 'https://anicloud.io/';

type EpisodeData = {
  animeId: number;
  name: string;
  season: number;
  episode: number;
  date: string;
  japaneseLanguage: boolean;
  newFlag: boolean;
};

const animeTable = tableDefinitions.animeTable;
const episodeTable = tableDefinitions.episodeTable;

const elementAnimeService = new ElementService<AnimeDbObject>();
const elementEpisodeService = new ElementService<EpisodeDbObject>();

export async function scrapNewEpisodesCron() {
  const currentAnimes: AnimeDbObject[] = await getAnimes();
  console.log(currentAnimes)
  const foundEpisodes = await scrapeNewEpisodes(currentAnimes);
  console.log(`Found ${foundEpisodes.length} new Episodes.`);
  if (foundEpisodes.length > 0) {
    await InsertEpisodes(foundEpisodes);
  }
}

async function getAnimes(): Promise<AnimeDbObject[]> {
  try {
    const animeList: AnimeDbObject[] = await elementAnimeService.GetAll(
      knex,
      animeTable.tableName
    );
    return animeList;
  } catch (err) {
    console.log(err);
    return err;
  }
}

async function scrapeNewEpisodes(
  animes: AnimeDbObject[]
): Promise<EpisodeData[]> {
  console.log('Start scraping...');

  const response = await axios.get(url);

  const $ = cheerio.load(response.data); // Load the HTML string into cheerio
  const animeEpisodesTable = $('.newEpisodeList > .row');
  const newAnimes: EpisodeData[] = [];

  animes.forEach((anime) => {
    animeEpisodesTable.each((i, elem) => {
      i;
      const name: string = $(elem).find('a strong').text(); // Parse the name
     
      if (
        anime.Expression.split(';').some((exp) =>
          name.toLowerCase().includes(exp.toLowerCase())
        )
      ) {
        const animeId: number = anime.Id;
        const season: number = parseInt(
          $(elem).find('a > .listTag').text().split(' ')[0].replace('S', '')
        ); // Parse the season
        const episode: number = parseInt(
          $(elem).find('a > .listTag').text().split(' ')[1]?.replace('E', '')
        ); // Parse the episode
        const date: string = $(elem).find('a .elementFloatRight').text(); // Parse the date
        const japaneseLanguage: boolean =
          $(elem).find('img.flag').attr('alt') === 'Deutsch Untertitel Flagge'
            ? true
            : false; // Parse the date
        const newFlag: boolean =
          $(elem).find('span.listTag.green').text() === 'Neu!' ? true : false; // Parse the date

        if (name !== '' && japaneseLanguage && newFlag) {
          newAnimes.push({
            animeId,
            name,
            season,
            episode,
            date,
            japaneseLanguage,
            newFlag,
          });
        }
      }
    });
  });

  return newAnimes;
}

async function InsertEpisodes(animeEpisodes: EpisodeData[]) {
  try {
    console.log('Write in DB ...');
    const timeElapsed = Date.now();
    const today = new Date(timeElapsed);
    
    animeEpisodes.forEach(async (elem) => {
      const episode: EpisodeDbObject = {
        AnimeId: elem.animeId,
        Season: elem.season,
        Episode: elem.episode,
        StatusId: 1, //TODO Enum
        Date: today
      };

      const exists = await elementEpisodeService.Exists(
        knex,
        episodeTable.tableName,
        [
          [episodeTable.animeId, episode.AnimeId.toString()],
          [episodeTable.episode, episode.Episode.toString()],
          [episodeTable.season, episode.Season.toString()],
        ]
      );

      if (exists[0].count == 0) {
        await elementEpisodeService.Insert<EpisodeDbObject>(
          knex,
          episodeTable.tableName,
          episode
        );
      }
    });
    console.log('Finished Db');
  } catch (err) {
    console.log(err);
    return err;
  }
}
