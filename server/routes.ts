import { Application } from 'express';
import animeRouter from './api/controllers/animes/router';
import episodeRouter from './api/controllers/episodes/router';
import userRouter from './api/controllers/user/router';
import authRouter from './api/controllers/auth/router';
import spotifyRouter from './api/controllers/spotify/router';
import personRouter from './api/controllers/person/router';
import fileRouter from './api/controllers/files/router';
export default function routes(app: Application): void {
  app.use('/', animeRouter);
  app.use('/', episodeRouter);
  app.use('/', userRouter);
  app.use('/', authRouter);
  app.use('/', spotifyRouter);
  app.use('/', personRouter);
  app.use('/', fileRouter);
}
