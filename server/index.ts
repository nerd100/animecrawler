import './common/env';
import Server from './common/server';
import { addJobs } from './jobs/jobs';
import routes from './routes';


const port = parseInt(process.env.PORT ?? '5000');
console.log('Register CronJobs');
export default new Server().router(routes).listen(port);
addJobs();
